import * as React from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, Text, View } from 'react-native'
import { Container, Row, Label } from './components/Styled'
import moment from 'moment'
// momnet 로컬 세팅
import 'moment/locale/ko' 

export default function App() {
  const [now, setNow] = React.useState(moment())

  // 1. 이 컴포넌트가 처음으로 화면에 표시될 때
  // 2. 주시하는 대상에 변화가 일어났을때

  React.useEffect( () => {
    setInterval( ()=> {
      setNow(moment())
    }, 1000)
  },[])

  return (

<Container>
  {/* <StatusBar style="auto" /> */}

  <Row>
    <Text>
      {now.format('YYYY년 MMM do (ddd)')}
    </Text>
  </Row>
  <Row>
    <Text>{now.format('HH')}</Text>
    <Text>{parseInt(now.format('s'))%2 == 0 ? ' ':':'}</Text>
    <Text>{now.format('mm')}</Text>
    <Text>{parseInt(now.format('s'))%2 == 0 ? ' ':':'}</Text>
    <Text>{now.format('ss')}</Text>
  </Row>
</Container>
  );
}
