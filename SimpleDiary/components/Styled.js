import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  flex: 1;
`;

const Content = styled.View`
  flex: 1;
`;

const TouchView = styled.TouchableOpacity`
  width: 100%;
  height: 50px;
  background-color: #000000;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
`;

const Button = (props) => {
  return(
    <TouchView onPress={props.onPress}>
      <Label>{props.children}</Label>
    </TouchView>
  )
}

export {Container, Content, Button}