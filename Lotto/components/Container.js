import styled from 'styled-components/native'
import Constants from 'expo-constants'

// 컨테이너 스타일
const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  justify-content: center;
  padding-top: ${Constants.statusBarHeight}px;
`
export default Container



