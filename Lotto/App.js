// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
import * as React from 'react'
import { Button, Dimensions, SafeAreaView, Text, View, StyleSheet } from 'react-native'
import _ from 'lodash'
import styled from 'styled-components/native'
import Container from './components/Container'
import Row from './components/Row'
import Ball from './components/Ball'
import Label from './components/Label'

// 1~45 랜덤 번호 생성
let numbers = [];
_.times( 45, n=>numbers.push(n+1))
// 섞는다
numbers = _.shuffle(numbers);

export default function App() {
  // 상태변화 state
  const [displayNumbers, setNumbers] = React.useState(_.shuffle(numbers)) // Hooks

  return (
    <Container>
      <Row>
        <Ball value={displayNumbers[0]}>
          <Label>{displayNumbers[0]}</Label>
        </Ball>
        <Ball value={displayNumbers[1]}>
          <Label>{displayNumbers[1]}</Label>
        </Ball>
        <Ball value={displayNumbers[2]}>
          <Label>{displayNumbers[2]}</Label>
        </Ball>
        <Ball value={displayNumbers[3]}>
          <Label>{displayNumbers[3]}</Label>
        </Ball>
        <Ball value={displayNumbers[4]}>
          <Label>{displayNumbers[4]}</Label>
        </Ball>
      </Row>
    </Container>
  )
}
